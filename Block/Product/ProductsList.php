<?php
namespace HumanElement\SingleCategoryProductWidget\Block\Product;

use Magento\Framework\Serialize\Serializer\Json;

class ProductsList extends \Magento\CatalogWidget\Block\Product\ProductsList
{
    const DEFAULT_SORT_BY = 'position';

    const DEFAULT_SORT_ORDER = 'asc';

    protected $_categoryFactory;

    protected $_productStatus;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        array $data = [],
        Json $json = null
    ) {
        $this->_categoryFactory = $categoryFactory;
        $this->_productStatus = $productStatus;

        parent::__construct($context, $productCollectionFactory, $catalogProductVisibility, $httpContext, $sqlBuilder,
            $rule, $conditionsHelper, $data, $json);
    }

    private function getCategory($categoryId){
        $category = $this->_categoryFactory->create()->load($categoryId);
        return $category;
    }

    //get products from a single category, sorted by their position in that category
    public function createCollection()
    {
        $categoryId = $this->getData('category_id');
        $count = $this->getData('products_count');

        $category = $this->getCategory($categoryId);

        $collection = $category->getProductCollection()
            ->addStoreFilter()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()])
            ->setPageSize($count)
            ->addAttributeToSort($this->getSortBy(), $this->getSortOrder());

        return $collection;
    }

    public function getSortBy()
    {
        if (!$this->hasData('products_sort_by')) {
            $this->setData('products_sort_by', self::DEFAULT_SORT_BY);
        }
        return $this->getData('products_sort_by');
    }

    public function getSortOrder()
    {
        if (!$this->hasData('products_sort_order')) {
            $this->setData('products_sort_order', self::DEFAULT_SORT_ORDER);
        }
        return $this->getData('products_sort_order');
    }
}