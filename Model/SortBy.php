<?php
namespace HumanElement\SingleCategoryProductWidget\Model;

class SortBy implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'position', 'label' => __('Position')]
        ];
    }
}